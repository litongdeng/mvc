<?php
/* Created by PhpStorm9.0.2, author:denglitong, datetime: 2015-10-10 12:49 */

//set content-type and charset
header('Content-Type:text/html; charset=utf-8');
//define the root path
define('ROOT_PATH', substr(dirname(__FILE__),0,-6));
//include the config file
include  ROOT_PATH . '/config/main.php';

//开启session
session_start();

//标志是后台入口
define('IS_ADMIN',true);

//路由控制器
Tool::urlManager();