<?php
/* Created by PhpStorm9.0.2, author:denglitong, datetime: 2015-10-10 13:03 */

/**
 * 管理后台默认控制器
 * 在这里继承的controller基类是属于controller/admin目录下的基类，autoload函数做了区分
 * */
class Defaultcontroller extends controller {

    public function index() {
        $this->render('default/index');
    }

}