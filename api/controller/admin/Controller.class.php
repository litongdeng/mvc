<?php
/* Created by PhpStorm9.0.2, author:denglitong, datetime: 2015-10-01 16:11 */

/**
 * 控制器基类
 * 这里前后台的controller基类分开，因为前后台控制器肯定需要分离，
 * 基类分开便于修改render redirect函数，它们引用的文件分属于前后台不同目录
 * */
class controller {

    /**
     * 生成url函数
     * @param $router, 如 'default/index'
     * @return string, url
     * */
    public static function createUrl($router) {
        return 'admin.php?r=' . $router;
    }

    /**
     * 视图渲染函数
     * @param $data,这是要传到html文件中使用的参数
     * @return null
     * */
     public static function render($tpl,$data=null) {
         //增加这一个循环将传过来的$data数组中的变量array(index => value)通过 $index就可以访问到，兼容之前的$data['index']也能访问到
         if (!empty($data)) {
             foreach ($data as $key => $value) {
                 $$key = $value;
             }
         }

        //因为这里是后台管理模块，所以对应的View层的目录要到admin目录下找
        $tplPath = VIEW_ADMIN_PATH . $tpl . '.php';
        if (!file_exists($tplPath)) {
            EException::throwException('The template file: ' . $tpl . '.php in ' . $tplPath . ' does not exist!');
        }
        //引入html文件
        include $tplPath;
    }

    /**
     * 重定向函数
     * @param $path,比如$this->redirect('user/login');则去到Usercontroller里面的login函数
     * */
     public static function redirect($path) {
        $path = 'admin.php?r=' . $path;
        //清除缓冲区，因为header()函数之前如果有输出则header函数会报错
        ob_clean();
        header('Location: ' . $path);
    }

}