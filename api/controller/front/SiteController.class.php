<?php
/* Created by PhpStorm9.0.2, author:denglitong, datetime: 2015-10-01 16:10 */

/**
 * 默认的控制器
 * 在这里继承的controller基类是属于controller/Front目录下的基类，autoload函数做了区分
 * */

class Sitecontroller extends controller {

    //controller默认会进入的index函数，由Tool::urlManager中定义的规则
    public function index() {
        $this->render('site/index');
    }

}