<?php
/* Created by PhpStorm9.0.2, author:denglitong, datetime: 2015-10-01 15:22 */

/**
 * 工具函数类
 * */
class Tool {

    /**
     * 路由管理器函数
     * index.php?r=controllerName/funcName 这个url会去到controllerName这个类执行funcName这个函数
     * */
    static public function urlManager() {
        //根据是前台还是后台对到对应的页面，因为前台后设置的默认控制器不一样
        //在new Abcontroller 或者new Abmodel的时候会触发autoload函数，而autoload函数对前后台做了区分，
        //所以在前后台不同的目录下时会创建分别对应前后台不同的类对象

        if (IS_ADMIN) {
            //如果没有指定控制器则默认去到 site/index
            $service = empty($_GET['r']) ?  'default/index' : $_GET['r'];
        } else {
            $service = empty($_GET['r']) ?  'site/index' : $_GET['r'];
        }
        $arr = explode('/',$service);
        //ucfirst() 首字母大写函数
        $className =  ucfirst($arr[0]) . 'controller';
        //如果没有指定函数则去到 index函数
        if (isset($arr[1])) {
            $methodName = trim($arr[1]) ? $arr[1] : 'index';
        } else {
            $methodName =  'index';
        }

        //正则表达式验证url是否合法 这里的controllerName，funcName只能是26个字母
        if (!preg_match('/([a-zA-Z])\/([a-zA-Z])/',$className . '/' .$methodName)) {
            EException::throwException('Illegal url!');
        }
        //验证url请求是否有对应的类和函数
        if (!class_exists($className)) {
            if (defined('CMS_DEGUB') && CMS_DEGUB) {//开启调试模式则报出错误信息
                EException::throwException('The ' . $className . ' class does not exit!');
            } else { //不开启调试则自动处理成默认的controller和method
                $className = IS_ADMIN ? 'Defaultcontroller' : 'Sitecontroller';
            }
        }
        if (!method_exists($className,$methodName)) {
            if (defined('CMS_DEGUB') && CMS_DEGUB) {//开启调试模式则报出错误信息
                EException::throwException('The method ' . $methodName . ' does not exit in class ' .$className . '!');
            } else { //不开启调试则自动处理成默认的controller和method
                $methodName = 'index';
            }
        }

        //根据类名$className创建一个类对象，这点php和其它语言有点不同
        $classObject = new $className();
        //调用call_user_func函数去执行$classObject类里面的$methodName函数
        call_user_func(array($classObject,$methodName));
    }

    /**
     * 过滤html标签
     * @param string $str,需要进行html标签过滤的字符串
     * @return string , 返回已经过滤了html标签的字符串
     * */
    static public  function htmlStringFilter($str) {
        $string = '';
        if (is_array($str)) {
            foreach ($str as $key => $value) {
                //递归调用解析多层数组,数组层数较少时用
                $string[$key] = Tool::htmlStringFilter($value);
            }
        } else if (is_object($str)) {
            $string=$str;
            foreach ($str as $key=>$value) {
                $string->$key = Tool::htmlStringFilter($value);
            }
        } else {
            //htmlspecialchars函数可以将字符串中的html标签过滤掉
            $string =  htmlspecialchars($str);
        }
        return $string;
    }


    /**
     * 过滤数据库查询语句
     * @param string $str,可能包含恶意代码的sql查询语句
     * @return string ,返回安全的查询语句
     * */
    static public function mysqlStringFilter($str){
        $string = '';
        if(is_array($str)) {
            foreach ($str as $key => $value) {
                $string[$key] = Tool::mysqlStringFilter($value);
            }
        }else if(is_object($str)){
            $string=$str;
            foreach($str as $key=>$value){
                $string->$key = Tool::mysqlStringFilter($value);
            }
        }else{
            //GPC是判断后的是否开启自动转义的常量，如果已开启自动转义则不再进行转义，否则需要进行转义
            //mysql_real_escape_string已被废弃，
            $string = GPC ? $str : mysqli_real_escape_string(DB::getMysqli(),$str);
        }
        return $string;
    }

    /**
     * 清除session
     * */
    static public function destroySession(){
        //开启session才销毁
        if (session_start()) {
            session_destroy();
        }
    }

}