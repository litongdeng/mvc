<?php
/* Created by PhpStorm9.0.2, author:denglitong, datetime: 2015-10-01 15:58 */

/**
 * 数据库连接类
 * */
class DB {

    //获取mysqli数据库对象句柄
    public static function getMysqli(){
        $mysqli = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAME); //连接数据库并且获取获取数据库对象句柄
        if(mysqli_connect_errno()) { //判断数据库连接是否正确
            echo '数据库连接错误！错误代码：'.mysqli_connect_errno();
            exit(0);
        }
        $mysqli->set_charset('utf8'); //设置编码集
        return $mysqli; //返回数据库资源句柄
    }

    /**
     * 关闭结果集和数据库资源句柄
     * 引用传参，传的是地址，否则传参传的是指针的副本，原来的指针还在，故函数里的销毁无效
     * */
    public static function unsetMysqli(&$result,$db){
        if(is_object($result)){
            //$result->free();//关闭结果集
            $result = null; //销毁结果集
        }
        if(is_object($db)){
            //$db->close();//关闭数据库
            $db = null;//销毁资源句柄
        }
    }

    //获取PDO数据库连接对象
    public static function getPDO() {
        try {
            $dsn = "mysql:host=" . DB_HOST . ";dbname=" . DB_NAME . ";";
            $pdo = new PDO($dsn,DB_USER,DB_PASSWORD,array(PDO::ATTR_PERSISTENT => true)); //使用持久化连接
            //持久的数据库连接是指在脚本结束运行时不关闭的连接。当收到一个持久连接的请求时。
            //PHP 将检查是否已经存在一个（前面已经开启的）相同的持久连接。如果存在，将直接使用这个连接；如果不存在，
            //则建立一个新的连接。所谓"相同"的连接是指用相同的用户名和密码到相同主机的连接。
            $pdo->exec("set names utf8");
            return $pdo;
        } catch (Exception $e) {
            die("Unable to connect: " . $e->getMessage());
        }
    }


}