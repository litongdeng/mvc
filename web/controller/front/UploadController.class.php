<?php
/* Created by PhpStorm9.0.2, author:denglitong, datetime: 2015-10-11 22:47 */

class Uploadcontroller extends controller {

    /**
     * 处理上传图片，重命名放到指定目录下，这里这个函数用来上传用户头像
     * */
    public function image() {
        //上传控件的名称是upload
        $fileUpload = new FileUpload('upload',$_POST['MAX_FILE_SIZE']);
        $ckefn = $_GET['CKEditorFuncNum'];
        $path = $fileUpload->getPath();
        $img = new Image($path);
        $img->ckeImg(650,0);
        $img->out();
        echo "<script>window.parent.CKEDITOR.tools.callFunction($ckefn,\"$path\", '图片上传成功')</script>";
    }

}