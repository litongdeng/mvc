<?php
/* Created by PhpStorm9.0.2, author:denglitong, datetime: 2015-10-01 15:42 */

/**
 * 主要配置文件
 * */

//数据库配置信息
define('DB_HOST','localhost');
define('DB_USER','root');
define('DB_PASSWORD','123456');
define('DB_NAME','db');

//判断自动转义是否开启
define('GPC',get_magic_quotes_gpc());

//定义public资源目录
define('PUBLIC_PATH',ROOT_PATH . '/public/');
//定义图片上传目录
define('UPLOAD_DIR','/public/upload/images/');
//定义前后台视图层目录
define('VIEW_FRONT_PATH',ROOT_PATH . '/view/front/');
define('VIEW_ADMIN_PATH',ROOT_PATH . '/view/admin/');

//要去extension/ckfinder/config.php修改$baseUrl为 '/mvc//web/public/upload/'

//定义最大上传文件的大小为10MB
define('MAX_FILE_SIZE',10);
//定义水印图片
define('MARK',ROOT_PATH.'/public/upload/images/shuiyin.png');


/**
 * 自动加载函数，对前后台不同模块的引入做了区分，
 * 比如 new abccontroller()，这时候程序会检测是否已经包含了这个类，如果没有则会执行__autoload函数，
 * 然后在__autoload函数里执行逻辑把abccontroller.class.php(包含abccontroller这个类)这个文件包含进来
 * */
function __autoload($className) {
    if (substr($className,-10)=='controller') {
        //如果是后台入口文件则引入后台模块的controller和后台模块的model,否则引入前台的controller和model
        if (IS_ADMIN) {
            require ROOT_PATH . '/controller/admin/' . $className . '.class.php';
        } else {
            require ROOT_PATH . '/controller/front/' . $className . '.class.php';
        }
    } else if (substr($className,-5)=='model') {
        if (IS_ADMIN) {
            require ROOT_PATH . '/model/admin/' . $className . '.class.php';
        } else {
            require ROOT_PATH . '/model/front/' . $className . '.class.php';
        }
    } else {
        //前后台共用core目录下的代码所以不需要做区分
        require ROOT_PATH . '/core/' . $className . '.class.php';
    }
}