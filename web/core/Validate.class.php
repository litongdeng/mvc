<?php
/* User: denglitong, Email:1525619747@qq.com, Date: 2015/4/16 0016  */
//验证类
class Validate{

    /**判断是否为空
     * @param $str
     * @return bool
     */
    static public function checkNull($str){
        return trim($str) ?  false : true;
    }

    /**判断长度是否合法
     * @param $str 要检测的字符串
     * @param $length 用来检测的长度
     * @param $flag 检测类型的标记，大于还是小于
     * @return bool 返回值，检测合法则返回真，检测不合法则返回假
     */
    static public function checkLength($str,$length,$flag) {
        if($flag=='min')
            return mb_strlen($str,'utf-8') < $length ?  true : false;
        else if($flag=='max')
            return mb_strlen($str,'utf-8') > $length ?  true : false;
        else if($flag=='equal')
            return mb_strlen(trim($str))==$length ? false : true;
        else
            Tool::alertBack('长度传值有误！');
    }
    //数据是否一致
    static public function checkEquals($str,$str_confirm){
        return trim($str)==trim($str_confirm) ?  true : false;
    }
    //session验证
    static public function checkSession() {
        if(!isset($_SESSION['admin'])){
            Tool::alertLocation('非法登录','admin_login.php');
        }
    }

    public static function checkEmailPattern($emailStr) {
        $pattern = '/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/';
        return preg_match($pattern,trim($emailStr));
        //var phone_pattern= /^(13[0-9]{9})|(15[0-9]{9})|(18[0-9]{9})$/;
    }
    public static function checkPhonePattern($phoneStr) {
        $pattern = '/^(13[0-9]{9})|(15[0-9]{9})|(18[0-9]{9})$/';
        return preg_match($pattern,trim($phoneStr));
    }
}