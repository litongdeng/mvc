<?php /*denglitong,1525619747@qq.com 2015/5/12 0012 下午 8:59 */
/**
 * 上传处理类
 * */
class FileUpload {

    private $error; //错误代码
    private $type; //类型
    ////类型合集
    private $typeArr = array('image/jpg','jpg','image/pjpg','pjgp',
        'image/jpeg','jpeg','image/pjpeg','pjpeg','image/png',
        'png','image/x-png','x-png','image/gif','gif');
    private $path; //目录路径
    private $today; //今天目录
    private $date; //当天时间
    private $name; //文件名
    private $tmp; //临时文件
    private $linkpath; //链接路径 根目录路径前后台通用
    private $maxsize;

    public function __construct($_file) {
        $this->error = $_FILES[$_file]['error'];
        $arr = explode('.',$_FILES[$_file]['name']);
        $this->type = $_FILES[$_file]['type']=='' ?  $arr[count($arr)-1]: $_FILES[$_file]['type'];

        $this->path = ROOT_PATH . UPLOAD_DIR;
        $this->date =  $this->path.date('Ym');
        $this->today = $this->path.date('Ym').'/'.date('d').'/';
        $this->name = $_FILES[$_file]['name'];
        $this->tmp = $_FILES[$_file]['tmp_name'];

        $this->checkType();
        $this->checkPath();
        $this->checkError();
        $this->moveUpload();
    }
    /**
     * 魔法函数——一定要是public
     */
    public function __set($key,$value) {
        $this->$key = $value;
    }
    public function __get($key) {
        return $this->$key;
    }

    /**
     * 返回路径
     * */
    public function getPath() {
        $dir = dirname(dirname($_SERVER['SCRIPT_NAME']));

        if($dir == '\\') $dir = '';
        $this->linkpath = $dir.$this->linkpath;
        return $this->linkpath;
    }

    /**
     * 移动文件
     * */
    private function moveUpload() {
        if(is_uploaded_file($this->tmp)){
            if (!move_uploaded_file($this->tmp,$this->setNewName())) {
                Tool::alertBackNoRefresh('临时文件移动失败');
            }
        }else{
            Tool::alertBackNoRefresh('临时文件不存在');
        }
    }

    /**
     * 设置新文件名
     */
    private function setNewName() {
        $nameArr = explode('.',$this->name);
        $postfix = $nameArr[count($nameArr)-1];
        $newname = date('YmdHis').mt_rand(100,1000).'.'.$postfix;
        $this->linkpath = UPLOAD_DIR.date('Ym').'/'.date('d').'/'.$newname;
        return $this->today.$newname;
    }

    /**
     * 验证目录
     * */
    private function checkPath() {
        if (!is_dir($this->path) || !is_writable($this->path)) {
            if (!mkdir($this->path)) {
                Tool::alertBackNoRefresh('警告：上传主目录创建失败');
            }
        }
        //public/images下面 年月 命名的文件夹
        if (!is_dir($this->date) || !is_writable($this->date)) {
            if (!mkdir($this->date)) {
                Tool::alertBackNoRefresh('警告：上传子目录创建失败');
            }
        }
        //public/images下面 年月 命名的文件夹下面的 日 命名的文件夹
        if (!is_dir($this->today) || !is_writable($this->today)) {
            if (!mkdir($this->today)) {
                Tool::alertBackNoRefresh('警告：上传子目录创建失败');
            }
        }
    }

    /**
     * 验证类型
     * */
    private function checkType() {
        if (!in_array($this->type,$this->typeArr)) {
            Tool::alertBackNoRefresh('不合法的上传类型');
        }
    }

    /**
     * 验证错误
     * */
    private function checkError() {
        if ($this->error!=0) {
            switch ($this->error) {
                case 1:
                    Tool::alertBackNoRefresh('警告：上传文件大小超过限定最大值');
                    break;
                case 2:
                    Tool::alertBackNoRefresh('警告：上传文件大小超过表单限定值'.$this->maxsize.'KB');
                    break;
                case 3:
                    Tool::alertBackNoRefresh('警告：很遗憾只有部分文件被上传');
                    break;
                case 4:
                    Tool::alertBackNoRefresh('警告：没有任何文件被上传');
                    break;
                default:
                    Tool::alertBackNoRefresh('警告：未知错误');
                    break;
            }
        }
    }


}
