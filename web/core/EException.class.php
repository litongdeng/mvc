<?php
/* Created by PhpStorm9.0.2, author:denglitong, datetime: 2015-10-01 15:23 */

/**
 * 异常处理类
 * */
class EException {

    /**
     * @overwrite
     * */
    static public function throwException($message){
        try {
            throw new Exception($message);
        } catch (Exception $e) {
            echo 'Error: ',  $e->getMessage(), "\n";
        }
        exit(0);
    }

}