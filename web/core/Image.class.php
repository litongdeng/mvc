<?php /*denglitong,1525619747@qq.com 2015/5/14 0014 上午 11:20 */
/**
 * 图像处理类，重命名，图像放缩，添加水印，保存到指定目录
 */
class Image {

    private $file;//图片地址硬路径
    private $width; //图片长度
    private $height; //图片高度
    public $type; //图片类型
    private $img; //原图资源句柄
    private $new; //新力资源句柄

    /**
     * 构造方法，初始化
     * */
    public function __construct($file) {
        //$this->file = $_SERVER['DOCUMENT_ROOT'] . substr($file,1); //本地wampserver
//        $this->file = $_SERVER['DOCUMENT_ROOT'] . $file; //本地Visual NMP
        $this->file = 'http://www.denglitong.com' . $file; //服务器虚拟主机
        list($this->width,$this->height,$this->type) = getimagesize($this->file);
        $this->img = $this->getFromImage($this->file,$this->type);
    }

    /*百分比缩放*/
/*    public function thumb($per){
        $new_width = $this->width * ($per / 100);
        $new_height = $this->height * ($per / 100);

        $this->new = imagecreatetruecolor($new_width,$new_height);
        imagecopyresampled($this->new,$this->img,0,0,0,0,$new_width,$new_height,$this->width,$this->height);
    }*/
    /*等比例缩放*/
/*    public function thumb($new_width,$new_height){
        //按照原图片长宽的大小比较来决定按哪条边进行缩放
        if($this->width < $this->height){
            $new_width = ($new_height / $this->height)*$this->width;
        }else{
            $new_height = ($new_width / $this->width)*$this->height;
        }
        $this->new = imagecreatetruecolor($new_width,$new_height);
        imagecopyresampled($this->new,$this->img,0,0,0,0,$new_width,$new_height,$this->width,$this->height);
    }*/

    /**
     * 缩略器（固定长高容器，图像等比例绽放，扩容填充，裁剪【固定了大小，不失真不变形】）
     */
    public function thumb($new_width,$new_height) {
        //创建一个容器
        $n_w = $new_width;
        $n_h = $new_height;
        //创建一个裁剪点
        $cut_width = 0;
        $cut_height = 0;
        //按照原图片长宽的大小比较来决定按哪条边进行缩放
        if ($this->width < $this->height) {
            $new_width = ($new_height / $this->height)*$this->width;
        } else {
            $new_height = ($new_width / $this->width)*$this->height;
        }
        if ($new_width < $n_w) {//如果新高度小于窗口高度
            $r = $n_w / $new_width;//求出容器缩放因子
            $new_width *= $r;
            $new_height *= $r;//对长宽做等比例缩放，扩张到窗口长宽
            $cut_height = ($new_height - $n_h) / 2;//确定裁剪点取出图片主要部分
        }
        if ($new_height < $n_h) {
            $r = $n_h / $new_height;
            $new_width *= $r;
            $new_height *= $r;
            $cut_width = ($new_width - $n_w) / 2;
        }

        $this->new = imagecreatetruecolor($n_w,$n_h);
        imagecopyresampled($this->new,$this->img,0,0,$cut_width,$cut_height,$new_width,$new_height,$this->width,$this->height);
    }

    /**
     * cke专用图像处理
     * */
    public function ckeImg($new_width,$new_height) {
        list($water_width,$water_height,$water_type) = getimagesize(MARK);
        $water = $this->getFromImage(MARK,$water_type);

        if ($this->width > $new_width) {
            $new_height = ($new_width / $this->width) * $this->height;
        } else {
            $new_width = $this->width;
            $new_height = $this->height;
        }
        if ($this->height > $new_height) {
            $new_width = ($new_height / $this->height) * $this->width;
        } else {
            $new_width = $this->width;
            $new_height = $this->height;
        }

        $water_x = $new_width-$water_width - 5;
        $water_y = $new_height-$water_height - 5;

        $this->new = imagecreatetruecolor($new_width,$new_height);
        imagecopyresampled($this->new,$this->img,0,0,0,0,$new_width,$new_height,$this->width,$this->height);
        if ($new_width > $water_width && $new_height > $water_height) {
            imagecopy($this->new,$water,$water_x,$water_y,0,0,$water_width,$water_height);
        }
    }

    /**
     * 加载各种类型的图片并返回资源类型
     */
    private function getFromImage($file,$type) {
        $img = '';
        switch($type){
            case 1:
                $img = imagecreatefromgif($file);
                break;
            case 2:
                $img = imagecreatefromjpeg($file);
                break;
            case 3:
                $img = imagecreatefrompng($file);
                break;
            default:
                Tool::alertBackNoRefresh('警告：此图片类型不支持');
                break;
        }
        return $img;
    }

    /**
     * 图像输出
     * */
    public function out() {
        //在没有尺寸压缩的情况下，
        //imagepng($this->new,$this->file); 这个写法里面不但没有压缩效果反倒还增加了图片少许的大小

        //使用这个压缩函数可以将图片多余的属性摘要去除，有一定压缩效果
        imagejpeg($this->new,$this->file,75);
        imagedestroy($this->img);
        imagedestroy($this->new);
    }

}
