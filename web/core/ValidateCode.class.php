<?php
/* User: denglitong, Email:1525619747@qq.com, Date: 2015/4/22 0022  */
//验证码类
class ValidateCode {

    private $charset = 'abcdefghklmnpqrstuvwxyzABCDEFGHKMNPQRSTUVWXYZ23456789'; //随机因子
    private $code;
    private $codeLength; //验证码长度
    private $width; //验证码图形宽度
    private $height; //高度
    private $img; //图形资源句柄
    private $font; //指定的字体
    private $fontsize; //指定的字体大小
    private $fontcolor; //字体颜色

    /**
     * 构造函数
     */
    public function __construct() {
        $this->codeLength = 4;
        $this->width = 130;
        $this->height = 50;
        $this->font = PUBLIC_PATH . 'font/elephant.ttf';
        $this->fontsize = 20;
    }
    /**
     * 生成随机码
     */
    private function createCode() {
        $len = strlen($this->charset)-1;
        for($i=0; $i<$this->codeLength; $i++){
            $this->code .= $this->charset[(mt_rand(0,$len))];
        }
    }
    /**
     * 生成背景
     */
    private function createBackground() {
        $this->img = imagecreatetruecolor($this->width,$this->height);
        $color = imagecolorallocate($this->img,
            mt_rand(157,255),mt_rand(157,255),mt_rand(157,255));
        imagefilledrectangle($this->img,0,$this->height,$this->width,0,$color);
    }
    /**
     * 生成文字
     */
    private function createFont() {
        $x = $this->width / $this->codeLength;
        $y = $this->height - 14;
        for ($i=0; $i<$this->codeLength; $i++) {
            $this->fontcolor = imagecolorallocate($this->img,
            mt_rand(0,156),mt_rand(0,156),mt_rand(0,156));
            imagettftext($this->img,$this->fontsize,mt_rand(-20,20),$x*$i,$y,$this->fontcolor,$this->font,$this->code[$i]);
        }
    }
    /**
     * 生成线条雪花
     */
    private function createLine() {
        for ($i=0; $i<6; $i++) {
            $color = imagecolorallocate($this->img,
                mt_rand(100,156),mt_rand(100,156),mt_rand(100,156));
            imageline($this->img,mt_rand(0,$this->width),mt_rand(0,$this->height),mt_rand(0,$this->width),mt_rand(0,$this->height),$color);
        }
        for ($i=0; $i<50; $i++) {
            $color = imagecolorallocate($this->img,
                mt_rand(200,255),mt_rand(200,255),mt_rand(200,255));
            imagestring($this->img,1,mt_rand(1,$this->width),mt_rand(1,$this->height),'*',$color);
        }
    }

    /**
     * 输出图形
     */
    private function output() {
        ob_clean(); //防止缓冲区里已有输出内容导致输出图像失败
        header('Content-type:image/png');
        imagepng($this->img);
        imagedestroy($this->img);
    }


    /**
     * 对外生成
     */
    public function outputCodeImg() {
        $this->createBackground();
        $this->createCode();
        $this->createFont();
        $this->createLine();
        $this->output();
    }

    /**
     * 获取验证码
     * @return string
     */
    public function getCode(){
        return strtolower($this->code);
    }

}