<?php
/* Created by PhpStorm9.0.2, author:denglitong, datetime: 2015-10-01 16:20 */

/**
 * 模型类基类，
 * 这里之所以将前后台的model层分开主要是因为这里的model层并不是真正意义上像框架那样的定义好对应数据表的模型层
 * 真正框架中的model层定义好数据表后(包括自动识别表的属性)还封装好了各种操作函数，这里我们需要的操作函数还要自己写，
 * 为避免前后台需要用到的model类函数写在同一个类文件里所以分开成前后台各有一个model层
 * (如果前后台的同一个model类函数写在同一个文件，一个是不容易区分前后台到底是哪些函数，另一个就是类会显得比较长)
 * */
class model {

    /**
     * 拦截器函数，其功能相当于JAVA里面对一个属性自动生成的setAttribute()和getAttribute()函数，
     * 只不过php把它写为魔法函数，即只需写一个函数就可以包含了所有的属性的set和get，属性可不预先定义都可以
     * */
    public function __set($key,$value) {
        $this->$key = Tool::mysqlStringFilter($value);
    }

    public function __get($key) {
        return $this->$key;
    }


    /**
     * 查找单个数据
     * @param string $sql
     * @return object,返回单条记录的对象
     * */
    protected function findOneBySql($sql) {
        $db = DB::getMysqli();
        $result = $db->query($sql);
        if ($result) { //如果查询结果集不为空
            $object = $result->fetch_object();
//            DB::unsetMysqli($result,$db); //$db为资源句柄类型所以不能通过引用传去进行销毁，直接在这里销毁
            unset($result);unset($db);
            return Tool::htmlStringFilter($object);
        }
        return null; //如果查询结果集为空,sql语句出错或者没有数据
    }

    /**
     * 查找多条数据
     * @param string $sql
     * @return object,返回多条记录的对象
     * */
    protected function findAllBySql($sql) {
        $db = DB::getMysqli();
        $result = $db->query($sql);//获取结果集
        if ($result) {
            $manage_arr = array();
            while (!!$object = $result->fetch_object()) {//面向对象获取数据
                $manage_arr[] =  $object;
            }
            unset($result);unset($db);
            return Tool::htmlStringFilter($manage_arr);
        }
        return null;
    }

    /**
     * 增加，删除，修改
     * @param string $sql
     * @return integer,返回影响行数
     * */
    protected function addUpdateDelete($sql) {
        $db = DB::getMysqli();
        $db->query($sql);
        $affected_rows = $db->affected_rows;
        if ($db->insert_id) {
            return $db->insert_id;
        }
        unset($db);
        return $affected_rows>0 ? true : false;
    }

    /**
     * 查找总记录条数
     * @param string $sql
     * @return integer,总记录条数
     * */
    protected function getTotalCount($sql) {
        $db = DB::getMysqli();
        $result = $db->query($sql);
        if ($result) {
            $total = $result->fetch_row();
            unset($result);unset($db);
            return $total[0];
        }
        return null;
    }

    /**
     * 执行多条语句
     * @param string $sql
     * @return bool,是束执行成功
     * */
    protected function executeMultipleSql($sql) {
        $db = DB::getMysqli();
        if ($db->multi_query($sql)) {
            unset($db);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param string $var,入参
     * @return string , 加了引号之后的返参
     * */
    /*protected static function quote($var) {
        $pdo = DB::getPDO();
        return $pdo->quote($var);
    }*/

    /**
     * 返回参数类型，在bindValue或者bindParam时要用到，防止有些地方有显示声明类型是导致执行失败
     * @param mixed $val, 入参
     * @return mixed PDO::PARAM_** 变量类型
     * */
    function selectDatatype($var) {
        if (is_bool($var)) {
            return PDO::PARAM_BOOL;
        } elseif (is_int($var)) {
            return PDO::PARAM_INT;
        } else if(is_null($var)) {
            return PDO::PARAM_NULL;
        } else {
            return PDO::PARAM_STR;
        }
    }

    /**
     *  pdo获取一行数据
     * @param string $sql, 传递过来的sql,可能带有参数
     * @param array $params, 传递过来的sql中的参数值的数组，array('placeholder' => 'value'),这里的placeholder参数点位符统一使用:id这种格式，$params默认为空数据
     * @return object ,返回值，是一行数据对应的对象实例
     * */
    public function fetchOne($sql, $params = array()) {
        $pdo = DB::getPDO();
        $stmt = $pdo->prepare($sql);
        if (is_array($params) && count($params)) {
            foreach ($params as $key => $value) {
                $stmt->bindValue($key,$value,$this->selectDatatype($value)); //绑定参数避免sql注入
            }
        }
        $stmt->execute();
        $object = $stmt->fetchObject();
        $stmt->closeCursor(); //为保证安全需要在下次执行前关闭游标
        return $object;
        //因为构造pdo的时候想用的就是pdo的持久化连接，所以不释放$pdo资源句柄
    }

    /**
     *  pdo获取多行数据
     * @param string $sql, 传递过来的sql,可能带有参数
     * @param array $params, 传递过来的sql中的参数值的数组，array('placeholder' => 'value'),这里的placeholder参数点位符统一使用:id这种格式，$params默认为空数据
     * @return object $returnObject ,返回值，是多行数据对应的对象实例组成的数组
     * */
    public function fetchAll($sql, $params = array()) {
        $pdo = DB::getPDO();
        $stmt = $pdo->prepare($sql);
        if (count($params)) {
            foreach ($params as $key => $value) {
                $stmt->bindValue($key,$value,$this->selectDatatype($value));
            }
        }
        $stmt->execute();
        //$stmt->debugDumpParams();
        $objects = $stmt->fetchAll(PDO::FETCH_OBJ);
        $stmt->closeCursor();
        return $objects;
    }

    /**
     *  pdo 执行一条insert update delete语句
     * @param string $sql, 传递过来的sql,可能带有参数
     * @param array $params, 传递过来的sql中的参数值的数组，array('placeholder' => 'value'),这里的placeholder参数点位符统一使用:id这种格式，$params默认为空数据
     * @param boolean $flag, 是否返回上一次影响的id，默认为false
     * @return integer ,返回影响的行数，执行失败返回0,如果有标志则返回上一次影响的id
     * */
    public function execute($sql, $params = array(), $flag = false) {
        $pdo = DB::getPDO();
        $stmt = $pdo->prepare($sql);
        if (count($params)) {
            foreach ($params as $key => $value) {
                $stmt->bindValue($key,$value);
            }
        }
        $stmt->execute();
        //$stmt->debugDumpParams();
        $rowCount = $stmt->rowCount();
        $stmt->closeCursor();
        return $flag ? $pdo->lastInsertId() : $rowCount;
    }

    /**
     *  pdo 执行多条insert update delete语句
     * @param string $sqlParamsArr, 传递过来的sql和参数数组
     * array (
     *          array('sql' => 'sql string', 'params' => array('placeholder' => 'paramValue'))  或者 array('sql string', array('placeholder' => 'paramValue')) ,
     *          array(),
     *          ...
     *        )
     * @return boolean ,执行成功返回真，执行失败返回flase
     * */
    public function multipleExecute($sqlParamsArr) {
        if (!(is_array($sqlParamsArr) && count($sqlParamsArr))) {
            EException::throwException('the second arguemnts of fetchOne() function is not correct');
        }
        $pdo = DB::getPDO();
        $pdo->beginTransaction();
        foreach ($sqlParamsArr as $sqlParams) {
            if (!(is_array($sqlParams) && count($sqlParams))) {
                EException::throwException('the second arguemnts of fetchOne() function is not correct');
            }
            $stmt = $pdo->prepare($sqlParams['sql']);
            if (is_array($sqlParams['params']) && count($sqlParams)) {
                foreach ($sqlParams['params'] as $key => $value) {
                    $stmt->bindValue($key,$value);
                    //bindValue和bindParam的区别！bindValue()直接将变量的值绑定到语句中， 而bindParam将变量作为引用进行绑定，并只在 PDOStatement::execute() 被调用的时候才取其值。
                    //多个参数循环后赋给第一个参数的值变成了后面的绑定变量的值从而导致类型出错导致sql执行出错
                    //同一组参数内多个占位符进行赋值的时候用bindValue(),针对不同组但组的类型相同时则用bindParam
                }
            }
            $stmt->execute();
            //$stmt->debugDumpParams();
            if (!$stmt->rowCount()) {
                $pdo->rollBack(); //某一条语句执行失败，回滚
                return false; //并返回false
            }
            $stmt->closeCursor(); //关闭游标
        }
        $pdo->commit();
        return true;
    }

    /**
     *  pdo 统计符合sql语句的行数
     * @param string $sql, 传递过来的sql,可能带有参数
     * @param array $params, 传递过来的sql中的参数值的数组，array('placeholder' => 'value'),这里的placeholder参数点位符统一使用:id这种格式，$params默认为空数据
     * @return integer ,返回sql查询的行数
     * */
    public function count($sql,$params) {
        return $this->execute($sql,$params);
        //因为对外的接口希望是execute只执行insert update delete的功能，甩以统计符合的行数放在这个函数来呈现给调用者
    }


}