<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>编辑</title>
    <script type="text/javascript" src="../extension/ckeditor/ckeditor.js"></script>
</head>
<body>

<form method="post" action="index.php?r=site/editor">
    <table cellspacing="0">

        <tr class="ckeditor">
            <td><textarea id="TextArea1" name="content" class="ckeditor"></textarea></td>
        </tr>

        <script type="text/javascript" src="../extension/ckfinder/ckfinder.js"></script>
        <script type="text/javascript">      // 启用 CKEitor 的上传功能，使用了 CKFinder 插件
        CKEDITOR.replace( 'TextArea1', {
            filebrowserBrowseUrl        : '../extension/ckfinder/ckfinder.html',
            filebrowserImageBrowseUrl   : '../extension/ckfinder/ckfinder.html?Type=Images',
            filebrowserFlashBrowseUrl   : '../extension/ckfinder/ckfinder.html?Type=Flash',
            filebrowserUploadUrl    : '../extension/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
            //原集成代码中默认的图片上传处理页面
            //filebrowserImageUploadUrl    : '../extension/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
            //修改后的图片上传处理页面，在这里给图片重命名并放在指定目录下
            filebrowserImageUploadUrl    : 'index.php?r=upload/image',

            filebrowserFlashUploadUrl      :  '../extension/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'     });
        </script>

        <tr>
            <td><input type="submit" name="send" value="提交" onclick="return checkEditor();"/></td>
        </tr>
    </table>
</form>

<script>
    function checkEditor() {
        if(CKEDITOR.instances.TextArea1.getData()==''){
            alert('内容不得为空');
            CKEDITOR.instances.TextArea1.focus();
            return false;
        }
        return true;
    }
</script>

</body>
</html>