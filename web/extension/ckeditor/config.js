﻿/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorconfig = function( config )
{
	config.language = 'zh-cn';
	config.uiColor = '#FFF';
	//对上传的图片做处理的脚本
	//config.filebrowserImageUploadUrl = '../config/ckeup.php?type=img';
};
